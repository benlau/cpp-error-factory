#include <iostream>
#include <sentry.h>
#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif
using namespace std;

int main()
{
    sentry_options_t *options = sentry_options_new();
    sentry_options_set_dsn(options, "https://examplePublicKey@o0.ingest.sentry.io/0");
    sentry_options_set_release(options, "my-project-name@2.3.12");
    sentry_init(options);

    cout << "Hello World!" << endl;

    sentry_capture_event(sentry_value_new_message_event(
    /*   level */ SENTRY_LEVEL_INFO,
    /*  logger */ "custom",
    /* message */ "It works!"
    ));
    
    sleep(5); /* Prevent to close immediately */

    sentry_close();

    return 0;
}
