FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y cmake libcurl4-gnutls-dev build-essential git && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

WORKDIR /code
